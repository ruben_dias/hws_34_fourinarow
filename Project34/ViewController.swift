//
//  ViewController.swift
//  Project34
//
//  Created by Ruben Dias on 21/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit
import GameplayKit

enum GameMode {
    case singlePlayer
    case multiPlayer
}

class ViewController: UIViewController {

    @IBOutlet var columnButtons: [UIButton]!
    
    var placedChips = [[UIView]]()
    var board: Board!
    
    var strategist: GKMinmaxStrategist!
    
    var gameMode: GameMode = .singlePlayer
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for _ in 0 ..< Board.width {
            placedChips.append([UIView]())
        }

        strategist = GKMinmaxStrategist()
        strategist.maxLookAheadDepth = 7
        strategist.randomSource = GKARC4RandomSource()
        
        resetBoard()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Switch to Multiplayer mode", style: .plain, target: self, action: #selector(toggleGameMode(sender:)))
    }

    @IBAction func makeMove(_ sender: UIButton) {
        let column = sender.tag

        if let row = board.nextEmptySlot(in: column) {
            board.add(chip: board.currentPlayer.chip, in: column)
            addChip(inColumn: column, row: row, color: board.currentPlayer.color)
            continueGame()
        }
    }
    
    @objc func toggleGameMode(sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Are you sure?", message: "This will reset the current game.", preferredStyle: .alert)
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        ac.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            switch self.gameMode {
            case .singlePlayer:
                self.gameMode = .multiPlayer
                self.navigationItem.rightBarButtonItem?.title = "Switch to Single Player mode"
            case .multiPlayer:
                self.gameMode = .singlePlayer
                self.navigationItem.rightBarButtonItem?.title = "Switch to Multiplayer mode"
            }
            
            self.resetBoard()
        }))
        
        present(ac, animated: true, completion: nil)
    }

    func resetBoard() {
        board = Board()
        
        if gameMode == .singlePlayer {
            strategist.gameModel = board
        } else {
            strategist.gameModel = nil
        }
        
        updateUI()

        for i in 0 ..< placedChips.count {
            for chip in placedChips[i] {
                chip.removeFromSuperview()
            }

            placedChips[i].removeAll(keepingCapacity: true)
        }
    }
    
    func addChip(inColumn column: Int, row: Int, color: UIColor) {
        let button = columnButtons[column]
        let size = min(button.frame.width, button.frame.height / 6)
        let rect = CGRect(x: 0, y: 0, width: size, height: size)

        if (placedChips[column].count < row + 1) {
            let newChip = UIImageView()
            newChip.frame = rect
            newChip.isUserInteractionEnabled = false
            newChip.image = UIImage(named: "chip")?.withRenderingMode(.alwaysTemplate)
            newChip.tintColor = color
            newChip.layer.cornerRadius = size / 2
            newChip.center = positionForChip(inColumn: column, row: row)
            newChip.transform = CGAffineTransform(translationX: 0, y: -800)
            view.addSubview(newChip)

            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                newChip.transform = CGAffineTransform.identity
            })

            placedChips[column].append(newChip)
        }
    }
    
    func positionForChip(inColumn column: Int, row: Int) -> CGPoint {
        let button = columnButtons[column]
        let size = min(button.frame.width, button.frame.height / 6)

        let xOffset = button.frame.midX
        var yOffset = button.frame.maxY - size / 2
        yOffset -= size * CGFloat(row)
        return CGPoint(x: xOffset, y: yOffset)
    }
    
    func continueGame() {
        var gameOverTitle: String? = nil

        if board.isWin(for: board.currentPlayer) {
            gameOverTitle = "\(board.currentPlayer.name) Wins!"
        } else if board.isFull() {
            gameOverTitle = "Draw!"
        }

        if gameOverTitle != nil {
            let alert = UIAlertController(title: gameOverTitle, message: nil, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Play Again", style: .default) { [unowned self] (action) in
                self.resetBoard()
            }

            alert.addAction(alertAction)
            present(alert, animated: true)

            return
        }

        board.currentPlayer = board.currentPlayer.opponent
        updateUI()
    }
    
    func updateUI() {
        title = "\(board.currentPlayer.name)'s Turn"
        
        if board.currentPlayer.chip == .blue {
            startAIMove()
        }
    }
    
    func startAIMove() {
        columnButtons.forEach { $0.isEnabled = false }

        let spinner = UIActivityIndicatorView(style: .medium)
        spinner.startAnimating()

        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: spinner)
        
        DispatchQueue.global().async { [unowned self] in
            let strategistTime = CFAbsoluteTimeGetCurrent()
            guard let column = self.columnForAIMove() else { return }
            let delta = CFAbsoluteTimeGetCurrent() - strategistTime

            let aiTimeCeiling = 1.0
            let delay = aiTimeCeiling - delta

            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                self.makeAIMove(in: column)
            }
        }
    }
    
    func columnForAIMove() -> Int? {
        if let aiMove = strategist.bestMove(for: board.currentPlayer) as? Move {
            return aiMove.column
        }

        return nil
    }
    
    func makeAIMove(in column: Int) {
        columnButtons.forEach { $0.isEnabled = true }
        navigationItem.leftBarButtonItem = nil
        
        if let row = board.nextEmptySlot(in: column) {
            board.add(chip: board.currentPlayer.chip, in: column)
            addChip(inColumn: column, row:row, color: board.currentPlayer.color)

            continueGame()
        }
    }
    
}

