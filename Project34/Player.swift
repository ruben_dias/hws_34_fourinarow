//
//  Player.swift
//  Project34
//
//  Created by Ruben Dias on 21/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit
import GameplayKit

class Player: NSObject, GKGameModelPlayer {
    
    static var allPlayers = [Player(chip: .red), Player(chip: .blue)]
    
    var chip: ChipColor
    var color: UIColor
    var name: String
    var playerId: Int
    
    var opponent: Player {
        if chip == .red {
            return Player.allPlayers[1]
        } else {
            return Player.allPlayers[0]
        }
    }

    init(chip: ChipColor) {
        self.chip = chip
        self.playerId = chip.rawValue

        if chip == .red {
            color = .systemRed
            name = "Red"
        } else {
            color = .systemBlue
            name = "Blue"
        }

        super.init()
    }
    
}
