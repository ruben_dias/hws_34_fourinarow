//
//  Move.swift
//  Project34
//
//  Created by Ruben Dias on 21/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import GameplayKit
import UIKit

class Move: NSObject, GKGameModelUpdate {
    
    var value: Int = 0
    var column: Int

    init(column: Int) {
        self.column = column
    }
}
